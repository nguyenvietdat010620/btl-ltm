package com.socket.demo.app.dto;

import com.socket.demo.domain.entity.Property;
import lombok.Data;

@Data
public class PropertyFilterDTO implements FilterDTO<Property>{
    private Long val;
}

package com.socket.demo.app.controller;

import com.socket.demo.app.dto.DTO;
import com.socket.demo.app.dto.FilterDTO;
import com.socket.demo.domain.service.BaseService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public abstract  class BaseController <O, ID, P1, FD extends FilterDTO<O>>{
    final Class<P1> responseClass;
    final Class<FD> filterDto;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    BaseService<O, ID> service;

    protected BaseController(Class<P1> responseClass, Class<FD> fpClass) {
        this.responseClass = responseClass;
        this.filterDto = fpClass;
    }

    @GetMapping()
    List<P1> findAll() {
        List<O> page = service.findAll();
        return page.stream().map(ele -> modelMapper.map(ele, responseClass)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    P1 getById(HttpServletRequest request, @PathVariable ID id) {
        O ob = service.findById(request, id);
        return modelMapper.map(ob, responseClass);
    }

    @PostMapping()
    P1 create(HttpServletRequest request, @Valid @RequestBody DTO dto) {
        O ob = service.create(request, dto);
        return modelMapper.map(ob, responseClass);
    }

    @PatchMapping("/{id}")
    P1 update(HttpServletRequest request, @PathVariable ID id, @Valid @RequestBody DTO dto) {
        O ob = service.update(request, id, dto);
        return modelMapper.map(ob, responseClass);
    }

    @DeleteMapping("/{id}")
    boolean delete(HttpServletRequest request, @PathVariable ID id) {
        return service.delete(request, id);
    }
}

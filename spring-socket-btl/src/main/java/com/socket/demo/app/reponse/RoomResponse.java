package com.socket.demo.app.reponse;

import lombok.Data;

@Data
public class RoomResponse {
    private Long id;
    private String code;
    private String name;
    private String description;
}

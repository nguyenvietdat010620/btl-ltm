package com.socket.demo.app.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
        property = "json_type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RoomDTO.class, name = "room"),
        @JsonSubTypes.Type(value = PropertyDTO.class, name = "property"),
})
public interface DTO<O> {
}

package com.socket.demo.app.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.socket.demo.domain.entity.Property;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonTypeName("property")
public class PropertyDTO extends BaseDTO implements DTO<Property> {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    @NotNull
    private Integer type;

    @NotNull
    private Long val;

    @NotNull
    private String roomCode;

    private String description;
}

package com.socket.demo.app.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.socket.demo.domain.entity.Room;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@JsonTypeName("room")
public class RoomDTO extends BaseDTO implements DTO<Room> {
	
	private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    private String description;
}

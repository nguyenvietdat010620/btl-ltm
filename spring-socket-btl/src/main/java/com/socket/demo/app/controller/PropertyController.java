package com.socket.demo.app.controller;
import com.socket.demo.app.dto.PropertyFilterDTO;
import com.socket.demo.app.dto.RoomFilterDTO;
import com.socket.demo.app.reponse.PropertyResponse;
import com.socket.demo.app.reponse.RoomResponse;
import com.socket.demo.domain.entity.Property;
import com.socket.demo.domain.entity.Room;
import com.socket.demo.domain.service.PropertyService;
import com.socket.demo.domain.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController("property")
@RequestMapping("/api/property")
public class PropertyController extends BaseController<Property, Long, PropertyResponse, PropertyFilterDTO>{

    @Autowired
    PropertyService propertyService;

    public PropertyController() {
        super(PropertyResponse.class, PropertyFilterDTO.class);
    }

    @GetMapping("/filter")
    List<PropertyResponse> filter(PropertyFilterDTO propertyFilterDTO) {
        return propertyService.filterProperty(propertyFilterDTO);
    }
}

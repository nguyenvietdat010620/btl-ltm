package com.socket.demo.app.controller;
import com.socket.demo.app.dto.RoomFilterDTO;
import com.socket.demo.app.reponse.RoomResponse;
import com.socket.demo.domain.entity.Room;
import com.socket.demo.domain.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/api/room")
public class RoomController extends BaseController<Room, Long, RoomResponse, RoomFilterDTO>{

    @Autowired
    RoomService roomService;

    public RoomController() {
        super(RoomResponse.class, RoomFilterDTO.class);
    }
}

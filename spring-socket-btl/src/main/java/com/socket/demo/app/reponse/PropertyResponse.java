package com.socket.demo.app.reponse;

import lombok.Data;

@Data
public class PropertyResponse {
    private Long id;
    private String code;
    private String name;
    private String description;
    private RoomResponse room;
    private Integer type;
    private Long val;
}

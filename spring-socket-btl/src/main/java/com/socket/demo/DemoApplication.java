package com.socket.demo;

import com.socket.demo.domain.service.PropertyService;
import com.socket.demo.domain.service.RoomService;
import com.socket.demo.domain.socket.SimpleServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(DemoApplication.class, args);
		SimpleServer server= new SimpleServer();
		RoomService roomService = applicationContext.getBean(RoomService.class);
		PropertyService propertyService = applicationContext.getBean(PropertyService.class);
		server.start(6667, roomService, propertyService);
	}

}

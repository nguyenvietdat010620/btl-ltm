package com.socket.demo.domain.repository;

import com.socket.demo.app.dto.PropertyFilterDTO;
import com.socket.demo.domain.entity.Property;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Long> {
    @Query(value = "select p from Property p where p.code = ?1 and p.id != ?2")
    Property findByCodeExceptId(String code, Long id);

    Property findByCode(String code);

    @Query(value = "select p from Property p where p.val >= ?1")
    List<Property> filterProperty(Long minimumVal);
}

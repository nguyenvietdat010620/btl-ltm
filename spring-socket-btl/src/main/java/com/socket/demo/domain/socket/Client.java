package com.socket.demo.domain.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.socket.demo.app.dto.PropertyDTO;
import com.socket.demo.app.dto.RoomDTO;
import com.socket.demo.domain.config.Constant;
import com.socket.demo.domain.entity.Property;
import com.socket.demo.domain.entity.Room;

public class Client {
    private static Socket clientSocket;
    private static PrintWriter out;
    private static BufferedReader in;

    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public String sendMessage(String msg) throws IOException {
        out.println(msg);
        String resp = in.readLine();
        return resp;
    }

    public static void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            Client client = new Client();
            client.startConnection("127.0.0.1", 6667);
            System.out.println("------------ Quản lí tài sản ----------");
            System.out.println("Mời bạn chọn: 1.Quản lí room    2.Quản lí property  3.Exit");
            String data = scanner.nextLine();
            while (!"3".equals(data)) {
                switch (data) {
                    case "1":
                        roomManagement(scanner, client);
                        break;
                    case "2":
                        propertyManagement(scanner, client);
                    default: break;
                }
                data = scanner.nextLine();
            }
            stopConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void roomManagement(Scanner scanner, Client client) {
        Gson gson = new Gson();
        System.out.println("------------ Quản lí room ----------");
        System.out.println("1.List room 2.Create room 3.Edit room 4.Delete room 5.Exit");
        try {
            String data = scanner.nextLine();
            while (!"5".equals(data)) {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                switch (data) {
                    case "1":
                        String response = client.sendMessage(Constant.LIST_ROOM);
                        List<Room> rooms = gson.fromJson(response, new TypeToken<List<Room>>() {
                        }.getType());
                        System.out.println("ID     Code    Name    Description");
                        rooms.forEach(item -> {
                            System.out.println(item.getId() + "   " + item.getCode() + "  " + item.getName() + "  " + item.getDescription());
                        });
                        ;
                        break;
                    case "2":
                        RoomDTO room = new RoomDTO();
                        room.setAction(Constant.INSERT_ROOM);
                        System.out.println("Code: ");
                        room.setCode(scanner.nextLine());
                        System.out.println("Name: ");
                        room.setName(scanner.nextLine());
                        System.out.println("Description: ");
                        room.setDescription(scanner.nextLine());
                        String response1 = client.sendMessage(gson.toJson(room));
                        System.out.println(response1);
                        break;
                    case "3":
                        RoomDTO room1 = new RoomDTO();
                        System.out.println("Id: ");
                        room1.setId(Long.valueOf(scanner.nextLine()));
                        room1.setAction(Constant.UPDATE_ROOM);
                        System.out.println("Code: ");
                        room1.setCode(scanner.nextLine());
                        System.out.println("Name: ");
                        room1.setName(scanner.nextLine());
                        System.out.println("Description: ");
                        room1.setDescription(scanner.nextLine());
                        String response11 = client.sendMessage(gson.toJson(room1));
                        System.out.println(response11);
                        break;
                    case "4":
                        System.out.println("Id: ");
                        String response2 = client.sendMessage(Constant.DELETE_ROOM + " " + scanner.nextLine());
                        System.out.println(response2);
                        break;
                    default:
                        break;
                }
                data = scanner.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void propertyManagement(Scanner scanner, Client client) {
        Gson gson = new Gson();
        System.out.println("------------ Quản lí property ----------");
        System.out.println("1.List property 2.Create property 3.Edit property 4.Delete property 5.Exit");
        try {
            String data = scanner.nextLine();
            while (!"5".equals(data)) {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                switch (data) {
                    case "1":
                        String response = client.sendMessage(Constant.LIST_PROPERTY);
                        List<Property> properties = gson.fromJson(response, new TypeToken<List<Property>>() {
                        }.getType());
                        System.out.println("ID     Code    Name    Room    Type   Value      Description");
                        properties.forEach(item -> {
                            System.out.println(item.getId() + "   " + item.getCode() + "  " + item.getName() + "  " + item.getRoom().getName() + "  " +
                                    item.getType() + "  " + item.getVal() + "  " + item.getDescription());
                        });
                        ;
                        break;
                    case "2":
                        PropertyDTO property = new PropertyDTO();
                        property.setAction(Constant.INSERT_PROPERTY);
                        System.out.println("Code: ");
                        property.setCode(scanner.nextLine());
                        System.out.println("Name: ");
                        property.setName(scanner.nextLine());
                        System.out.println("Room code: ");
                        property.setRoomCode(scanner.nextLine());
                        System.out.println("Type: ");
                        property.setType(Integer.parseInt(scanner.nextLine()));
                        System.out.println("Value: ");
                        property.setVal(Long.valueOf(scanner.nextLine()));
                        System.out.println("Description: ");
                        property.setDescription(scanner.nextLine());
                        String response1 = client.sendMessage(gson.toJson(property));
                        System.out.println(response1);
                        break;
                    case "3":
                        PropertyDTO property1 = new PropertyDTO();
                        System.out.println("Id: ");
                        property1.setId(Long.valueOf(scanner.nextLine()));
                        property1.setAction(Constant.UPDATE_PROPERTY);
                        System.out.println("Code: ");
                        property1.setCode(scanner.nextLine());
                        System.out.println("Name: ");
                        property1.setName(scanner.nextLine());
                        System.out.println("Room code: ");
                        property1.setRoomCode(scanner.nextLine());
                        System.out.println("Type: ");
                        property1.setType(Integer.parseInt(scanner.nextLine()));
                        System.out.println("Value: ");
                        property1.setVal(Long.valueOf(scanner.nextLine()));
                        System.out.println("Description: ");
                        property1.setDescription(scanner.nextLine());
                        String response2 = client.sendMessage(gson.toJson(property1));
                        System.out.println(response2);
                        break;
                    case "4":
                        System.out.println("Id: ");
                        String response3 = client.sendMessage(Constant.DELETE_PROPERTY + " " + scanner.nextLine());
                        System.out.println(response3);
                        break;
                    default:
                        break;
                }
                data = scanner.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

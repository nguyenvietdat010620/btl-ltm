package com.socket.demo.domain.service;

import com.socket.demo.app.dto.DTO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface BaseService<T, S> {

    List<T> findAll();

    T findById(HttpServletRequest request, S id);

    @Transactional(rollbackFor = Exception.class)
    T create(HttpServletRequest request, DTO dto);

    @Transactional(rollbackFor = Exception.class)
    T update(HttpServletRequest request, S id, DTO dto);

    T update(HttpServletRequest request, Long id, DTO dto);

    @Transactional(rollbackFor = Exception.class)
    boolean delete(HttpServletRequest request, S id);
}

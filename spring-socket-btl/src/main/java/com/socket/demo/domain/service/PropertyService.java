package com.socket.demo.domain.service;

import com.socket.demo.app.dto.DTO;
import com.socket.demo.app.dto.PropertyDTO;
import com.socket.demo.app.dto.PropertyFilterDTO;
import com.socket.demo.app.reponse.PropertyResponse;
import com.socket.demo.domain.entity.Property;
import com.socket.demo.domain.entity.Room;
import com.socket.demo.domain.exceptions.BusinessException;
import com.socket.demo.domain.repository.PropertyRepository;
import com.socket.demo.domain.repository.RoomRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PropertyService implements BaseService<Property, Long>{

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PropertyRepository propertyRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Override
    public List<Property> findAll() {
        List<Property> properties = propertyRepository.findAll();
        messagingTemplate.convertAndSend("/property/list", properties);
        return properties;
    }

    @Override
    public Property findById(HttpServletRequest request, Long id) {
        return propertyRepository.findById(id).get();
    }

    @Override
    public Property create(HttpServletRequest request, DTO dto) {
        PropertyDTO propertyDTO = modelMapper.map(dto, PropertyDTO.class);
        Property property = propertyRepository.findByCode(propertyDTO.getCode());
        if (property != null) {
            throw new BusinessException(HttpStatus.BAD_GATEWAY, "Property code exist");
        }
        property = new Property();
        Room room = getRoomByCode(propertyDTO.getRoomCode());
        property.setCode(propertyDTO.getCode());
        property.setDescription(propertyDTO.getDescription());
        property.setType(propertyDTO.getType());
        property.setVal(propertyDTO.getVal());
        property.setName(propertyDTO.getName());
        property.setRoom(room);
        property =  propertyRepository.save(property);
        findAll();
        return property;
    }

    Room getRoomByCode (String code) {
        Room room = roomRepository.findByCode(code);
        if (room == null)
            throw new BusinessException(HttpStatus.NOT_FOUND, "Room not exist");
        return room;
    }

    @Override
    public Property update(HttpServletRequest request, Long id, DTO dto) {
        PropertyDTO propertyDTO = modelMapper.map(dto, PropertyDTO.class);
        Property property = propertyRepository.findByCodeExceptId(propertyDTO.getCode(), id);
        if (property != null) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "Property code exist");
        }
        property = propertyRepository.getReferenceById(id);
        Room room = getRoomByCode(propertyDTO.getRoomCode());
        property.setRoom(room);
        property.setCode(propertyDTO.getCode());
        property.setDescription(propertyDTO.getDescription());
        property.setType(propertyDTO.getType());
        property.setVal(propertyDTO.getVal());
        property.setName(propertyDTO.getName());
        property = propertyRepository.save(property);
        findAll();
        return property;
    }

    @Override
    public boolean delete(HttpServletRequest request, Long id) {
        propertyRepository.deleteById(id);
        findAll();
        return true;
    }

    public List<PropertyResponse> filterProperty(PropertyFilterDTO propertyFilterDTO) {
        return propertyRepository.filterProperty(propertyFilterDTO.getVal()).stream().map(ele -> modelMapper.map(ele, PropertyResponse.class)).collect(Collectors.toList());
    }
}

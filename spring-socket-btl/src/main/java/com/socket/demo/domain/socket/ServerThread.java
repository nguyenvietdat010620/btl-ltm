package com.socket.demo.domain.socket;

import com.google.gson.Gson;
import com.socket.demo.app.dto.BaseDTO;
import com.socket.demo.app.dto.PropertyDTO;
import com.socket.demo.app.dto.RoomDTO;
import com.socket.demo.domain.config.Constant;
import com.socket.demo.domain.entity.Property;
import com.socket.demo.domain.entity.Room;
import com.socket.demo.domain.service.PropertyService;
import com.socket.demo.domain.service.RoomService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class ServerThread extends Thread {
    private Socket socket;

    private RoomService roomService;

    private PropertyService propertyService;

    public ServerThread(Socket socket, RoomService roomService, PropertyService propertyService) {
        this.socket = socket;
        this.roomService = roomService;
        this.propertyService = propertyService;
    }

    public void run() {
            Gson gson = new Gson();
            try {
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                while (true) {
                    String data = in.readLine();
                    try {
                        if (data.startsWith(Constant.LIST_ROOM)) {
                            List<Room> roomList = roomService.findAll();
                            out.println(gson.toJson(roomList));
                        } else if (data.startsWith(Constant.DELETE_ROOM)) {
                            String roomId = data.split(" ")[1];
                            roomService.delete(null, Long.valueOf(roomId));
                            out.println("Oke");
                        } else if (data.startsWith(Constant.LIST_PROPERTY)) {
                            List<Property> propertyList = propertyService.findAll();
                            out.println(gson.toJson(propertyList));
                        } else if (data.startsWith(Constant.DELETE_PROPERTY)) {
                            String propertyId = data.split(" ")[1];
                            propertyService.delete(null, Long.valueOf(propertyId));
                            out.println("Oke");
                        }
                        else if (isJson(data)) {
                            BaseDTO baseDTO = gson.fromJson(data, BaseDTO.class);
                            switch(baseDTO.getAction()) {
                                case Constant.INSERT_ROOM -> {
                                    RoomDTO roomDTO =  gson.fromJson(data, RoomDTO.class);
                                    roomService.create(null, roomDTO);
                                    out.println("Oke");
                                }
                                case Constant.UPDATE_ROOM -> {
                                    RoomDTO roomDTO =  gson.fromJson(data, RoomDTO.class);
                                    roomService.update(null, roomDTO.getId(), roomDTO);
                                    out.println("Oke");
                                }
                                case Constant.INSERT_PROPERTY -> {
                                    PropertyDTO propertyDTO =  gson.fromJson(data, PropertyDTO.class);
                                    propertyService.create(null, propertyDTO);
                                    out.println("Oke");
                                }
                                case Constant.UPDATE_PROPERTY -> {
                                    PropertyDTO propertyDTO =  gson.fromJson(data, PropertyDTO.class);
                                    propertyService.update(null, propertyDTO.getId(), propertyDTO);
                                    out.println("Oke");
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        out.println(e.getMessage());
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

    }

    private static boolean isJson(String Json) {
        try {
            new JSONObject(Json);
        } catch (JSONException ex) {
            try {
                new JSONArray(Json);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}

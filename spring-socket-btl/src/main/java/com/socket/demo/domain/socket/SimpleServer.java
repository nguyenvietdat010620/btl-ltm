package com.socket.demo.domain.socket;

import com.socket.demo.domain.service.PropertyService;
import com.socket.demo.domain.service.RoomService;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServer {

    private ServerSocket serverSocket;

    public void start(int port, RoomService roomService, PropertyService propertyService) {
        try {
            System.out.println("Socket run");
            serverSocket = new ServerSocket(port);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("New client connected");
                new ServerThread(clientSocket, roomService, propertyService).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }
}

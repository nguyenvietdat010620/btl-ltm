package com.socket.demo.domain.config;

public class Constant {

	public final static String LIST_ROOM = "LIST_ROOM";
	public final static String INSERT_ROOM = "INSERT_ROOM";
	public final static String UPDATE_ROOM = "UPDATE_ROOM";

	public final static String DELETE_ROOM = "DELETE_ROOM";

	public final static String LIST_PROPERTY = "LIST_PROPERTY";

	public final static String INSERT_PROPERTY = "INSERT_PROPERTY";

	public final static String UPDATE_PROPERTY = "UPDATE_PROPERTY";

	public final static String DELETE_PROPERTY = "DELETE_PROPERTY";
}

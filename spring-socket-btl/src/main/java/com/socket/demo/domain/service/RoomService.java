package com.socket.demo.domain.service;

import com.socket.demo.app.dto.DTO;
import com.socket.demo.app.dto.RoomDTO;
import com.socket.demo.domain.entity.Room;
import com.socket.demo.domain.exceptions.BusinessException;
import com.socket.demo.domain.repository.RoomRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoomService implements BaseService<Room, Long>{
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    @Override
    public List<Room> findAll() {
        List<Room> rooms = roomRepository.findAll();
        messagingTemplate.convertAndSend("/room/list", rooms);
        return rooms;
    }

    @Override
    public Room findById(HttpServletRequest request, Long id) {
        return roomRepository.getReferenceById(id);
    }

    @Override
    public Room create(HttpServletRequest request, DTO dto) {
        RoomDTO roomDTO = modelMapper.map(dto, RoomDTO.class);
        Room room = roomRepository.findByCode(roomDTO.getCode());
        if (room != null) {
            throw new BusinessException(HttpStatus.BAD_GATEWAY, "Room code exist");
        }
        room = Room.builder().code(roomDTO.getCode()).name(roomDTO.getName()).description(roomDTO.getDescription()).build();
        room = roomRepository.save(room);
        findAll();
        return room;
    }

    @Override
    public Room update(HttpServletRequest request, Long id, DTO dto) {
        RoomDTO roomDTO = modelMapper.map(dto, RoomDTO.class);
        Room room = roomRepository.getReferenceById(id);
        if (room == null) {
            throw new BusinessException(HttpStatus.BAD_GATEWAY, "Room not exist");
        }
        room.setCode(roomDTO.getCode());
        room.setDescription(roomDTO.getDescription());
        room.setName(roomDTO.getName());
        room = roomRepository.save(room);
        findAll();
        return room;
    }

    @Override
    public boolean delete(HttpServletRequest request, Long id) {
        roomRepository.deleteById(id);
        findAll();
        return true;
    }
}
